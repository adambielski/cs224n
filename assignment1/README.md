To install dependencies in virtualenv run
```
virtualenv .env
source .env/bin/activate
pip install -r requirements
```